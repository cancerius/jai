
package WSTIE;
//import java.io.UnsupportedEncodingException;
import java.util.Scanner; 
import org.apache.commons.text.*;
import org.apache.commons.text.translate.*;
import org.apache.commons.text.similarity.*;

//Jerzy Kaczmarczyk


public class App 
{
    public static void main( String[] args ) 
    {
    
    	
    	Scanner scan = new Scanner(System.in);
    	
    	CharSequence test = "LoremIpsum";
    	System.out.print("Podaj pierwszy String: ");
    	String a = scan.next();
    	System.out.print("Podaj drugi String: ");
    	String b = scan.next();
    	HammingDistance distance = new HammingDistance();
        System.out.print("HammingDistance: ");
        System.out.println(distance.apply("testujemy", "tessujeyy"));
        
    
        JaccardDistance jaccard = new JaccardDistance();
        System.out.print("JaccardDistance: ");
        System.out.println(jaccard.apply(a, b));
        
        LevenshteinDistance LevenshteinDistance = new LevenshteinDistance();
        System.out.print("LevenshteinDistance: ");
        System.out.println(LevenshteinDistance.apply(a, b));
        
        StringBuilder nowy = new StringBuilder();
        nowy.append("aTakiSobie");
        StrBuilder StrBuilder = new StrBuilder();
        StrBuilder.append("aTakiSobieText");
        System.out.print("StrBuilder capacity: ");
        System.out.println(StrBuilder.capacity());
        
        UnicodeEscaper UnicodeEscaper = new UnicodeEscaper();
        System.out.print("UnicodeEscaper HashCode: ");
        System.out.println(UnicodeEscaper.hashCode());
        
        System.out.println("UnicodeEscaper Translate charsequence to string");
        UnicodeEscaper.translate(test);
        
        JaccardSimilarity jaccardS = new JaccardSimilarity();
        System.out.print("JaccardDSimilarity: ");
        System.out.println(jaccardS.apply(a,b));
    }
}
